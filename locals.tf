# defines variables per environment
# Fill out each VAULT_ADDRESS with the the available vault address
locals {
    env = {
        non-prod = {
            VAULT_ADDRESS = ""
        }
        production = {
            VAULT_ADDRESS = ""
        }
    }

    # This will search the locals, if a terraform worspace is not selected, then the non-prod variables will be used as default.
    env_vars = "${contains(keys(local.env), terraform.workspace) ? terraform.workspace : "non-prod" }"
}