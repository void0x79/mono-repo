# ----------------------- Calling Namespace Modules ------------------------

module "example_namespace_one" {
    source = "./Modules/namespaces"
    namespace_path = "example_namespace_path_one"

    tags = {
        Terraform   = "true"
        Environment = "Global"
    }
}

module "example_namespace_two" {
  source = "./Modules/namespaces"
  namespace_path = "example_namespace_path_two"

  tags = {
      Terraform   = "true"
      Environment = "Global"
  }
}

# ------------------------- Calling ldap Authentication Method ---------------------

module "example_ldap_auth" {
    source        = "./Modules/auth_methods/ldap"
    ldap_url      = # Enter ldap url
    ldap_userdn   = # Enter ldap dn 
    ldap_userattr = # Enter userattribute
    upndomain     = # Enter upndomain
    discoverdn    = # Enter discoverdn
    groupdn       = # Enter groupdn
    groupfilter   = # Enter group filter
}