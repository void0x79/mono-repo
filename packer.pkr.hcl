packer {
    required_plugins {
        amazon = {
            version = ">= 0.0.1"
            source = "github.com/hashicorp/amazon"
        }
    }
}
################## VMWare-Iso ##################
source "vmware-iso" "hashi-on-vmware" {
    iso_url          = ""
    iso_checksum     = ""
    ssh_username     = "packer"
    ssh_password     = "packer"
    shutdown_command = " shutdown -P now" 
}

################## IMAGE DEFINITION ################
source "amazon-ebs" "adp_rhel_image" {
    region          = "us-east-1"
    instance_type   = "${var.INSTANCE_TYPE}"
    ssh_username    = "${var.SSH_USERNAME}"
    source_ami_filter {
        filters     = {
            virtualization-type = "hvm"
            name = "Red Hat 7"
            root-device-type = "ebs"
        }
        owners      = ["${var.OWNER_ID}"]
        most_recent = "${var.MOST_RECENT_BOOL}"
    }
    tags = {
        Builder = "Packer"
    }
}

build {

    source = [ "vmware-iso.hashi-on-vmware"] {
        ........... <DO something here>
    }


    source "amazon-ebs.adp_rhel_image" {
        ami_regions = ["us-east-1", "us-west-2"]
        ami_name        = "${var.AMI_NAME}_Vault"
    }
    
    provisioner "file" {
        source      = "provisioner/"
        destination = "/tmp/"
    }

    provisioner "shell" {
        environment_vars = [
            "PRIVATE_IP=$(curl http://169.254.169.254/latest/meta-data/local-ipv4)",
            "NODENAME=$(hostname)",
            "VAULT_API_ADDRESS=${var.VAULT_API_ADDRESS}",
            "VAULT_TAG=${var.VAULT_TAG}",
            "CONSUL_TAG=${var.CONSUL_TAG}"
        ]
        inline = [ 
            "sed -i 's/VAULT_API_ADDRESS/$VAULT_API_ADDRESS/g' /tmp/config_files/vault.hcl",
            "sed -i 's/VAULT_CLUSTER_ADDRESS/$PRIVATE_IP:8201/g' /tmp/config_files/vault.hcl",
            "sed -i 's/NODENAME/$NODENAME/g' /tmp/config_files/consul.json",
            "sed -i 's/PRIVATE_IP/$PRIVATE_IP/g' /tmp/config_files/consul.json",
            "sudo bash /tmp/install.sh vault-server",
            "curl -O ",
            "curl -O ",
            "unzip vault_$VAULT_TAG+ent_linux_amd64.zip && unzip consul_$CONSUL_TAG+ent_linux_amd64.zip",
            "mv vault /usr/local/bin && mv consul /usr/local/bin",
            "rm vault_$VAULT_TAG+ent_linux_amd64.zip && rm consul_$CONSUL_TAG+ent_linux_amd64.zip"
        ]
    }
}

build {
    source "amazon-ebs.adp_rhel_image" {
        ami_regions = ["us-east-1", "us-west-2"]
        ami_name        = "${var.AMI_NAME}_Consul"
    }
    
    provisioner "file" {
        source      = "provisioner/"
        destination = "/tmp/"
    }

    provisioner "shell" {
        environment_vars = [
            "PRIVATE_IP=$(curl http://169.254.169.254/latest/meta-data/local-ipv4)",
            "NODENAME=$(hostname)",
            "VAULT_API_ADDRESS=${var.VAULT_API_ADDRESS}",
            "CONSUL_TAG=${var.CONSUL_TAG}"
        ]
        inline = [
            "sed -i 's/NODENAME/$NODENAME/g' /tmp/config_files/consul.json",
            "sed -i 's/PRIVATE_IP/$PRIVATE_IP/g' /tmp/config_files/consul.json",
            "sudo bash /tmp/install.sh consul-server", 
            "curl -O ",
            "unzip vault_$VAULT_TAG+ent_linux_amd64.zip && unzip consul_$CONSUL_TAG+ent_linux_amd64.zip",
            "mv consul /usr/local/bin",
            "rm consul_$CONSUL_TAG+ent_linux_amd64.zip"
        ]
    }
}
