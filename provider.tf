provider "vault" {
  address = "${local.workspace["VAULT_ADDRESS"]}"
  token = # 3 ways to set the token. 1 - in ~/.vault-token.  2 - environment variable (VAULT_TOKEN) or 3 - hardcoded here (bad idea)
}

